package ejercicio6;

public class Camion extends Vehiculo {
	
	public static final Double RECARGO = 1.15d;
	

	public Camion(Duenio duenio, String matricula, String modelo, String averia, Double horasEstimadas) {
		super(duenio, matricula, modelo, averia, horasEstimadas);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Double calcularPrecio() {
		
		return super.calcularPrecio() * Camion.RECARGO;
	}

}
