package ejercicio6;

public abstract class Vehiculo {
	private Duenio duenio;
	private String matricula;
	private String modelo;
	private String averia;
	private Double horasEstimadas;
	private Double precioRespuesto;
	
	public Vehiculo(Duenio duenio, String matricula, String modelo, String averia, Double horasEstimadas) {
		
		this.duenio = duenio;
		this.matricula = matricula;
		this.modelo = modelo;
		this.averia = averia;
		this.horasEstimadas = horasEstimadas;
		this.precioRespuesto = 0d;
	}
	
	public  Double calcularPrecio()
	{
		return (this.getHorasEstimadas() * 70d ) + this.precioRespuesto; 
	};

	@Override
	public String toString() {
		return "Vehiculo [duenio=" + duenio + ", matricula=" + matricula + ", modelo=" + modelo + ", averia=" + averia
				+ ", horasEstimadas=" + horasEstimadas + ", precioRespuesto=" + precioRespuesto + "]";
	}
	//=== setters y getters

	public Duenio getDuenio() {
		return duenio;
	}

	public void setDuenio(Duenio duenio) {
		this.duenio = duenio;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getAveria() {
		return averia;
	}

	public void setAveria(String averia) {
		this.averia = averia;
	}

	public Double getHorasEstimadas() {
		return horasEstimadas;
	}

	public void setHorasEstimadas(Double horasEstimadas) {
		this.horasEstimadas = horasEstimadas;
	}

	public Double getPrecioRespuesto() {
		return precioRespuesto;
	}

	public void setPrecioRespuesto(Double precioRespuesto) {
		this.precioRespuesto = precioRespuesto;
	}
	

}
