package ejercicio6;

public class ApliEjercicio6 {

	public static void main(String[] args) {
		Duenio due1 = new Duenio("Juan","Janevski",4486505,"Galina",495);
		Auto auto1 = new Auto(due1,"AB002DX","RENAULT","FRENOS",12D);
		
		auto1.setPrecioRespuesto(13500d);
		
		System.out.println(auto1.toString());

	}

}
