package ejercicio6;

public class Auto extends Vehiculo {
	//
	
	public final static Double RECARGO = 1.05d ; 
	
	public Auto(Duenio duenio, String matricula, String modelo, String averia, Double horasEstimadas) {
		super(duenio, matricula, modelo, averia, horasEstimadas);
		
	}

	@Override
	public Double calcularPrecio() {
		// TODO Auto-generated method stub
		return super.calcularPrecio() * Auto.RECARGO;
	}

}
