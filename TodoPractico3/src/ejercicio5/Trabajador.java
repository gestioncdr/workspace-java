package ejercicio5;

import java.time.LocalDate;

public class Trabajador extends Empleado {
	private Double sueldo;
	private Double retenciones;
	private Double impuestos;
	private Double premios;
	
	//=== constructores ================================
	
	public Trabajador() {
		// TODO Auto-generated constructor stub
		super();
		this.setSueldo(0d);
		this.setRetenciones(0d);
		this.setImpuestos(0d);
		this.setPremios(0d);
	}

	public Trabajador(Integer nroLegajo, String nombre, String puesto, String direccion, LocalDate fechaNacimiento,
			LocalDate fechaContratacion, Double sueldo, Double retenciones,
			Double impuestos, Double premios) {
		super(nroLegajo, nombre, puesto, direccion, fechaNacimiento, fechaContratacion);
		this.setSueldo(sueldo);
		this.setRetenciones(retenciones);
		this.setImpuestos(impuestos);
		this.setPremios(premios);
		
		
	}
	
	// === m�todos ======================================
	
	@Override
	public Double calcularSueldo() {
		
		return this.getSueldo() - this.getRetenciones() - this.getImpuestos() + this.getPremios();
	}
	
	@Override
	public String toString() {
		return super.toString() + "\n"+ "Trabajador [sueldo=" + sueldo + ", retenciones=" + retenciones + ", impuestos=" + impuestos
				+ ", premios=" + premios + "]";
	}
	
	
	//=== setters y getters =============================


	public Double getSueldo() {
		return sueldo;
	}

	public void setSueldo(Double sueldo) {
		this.sueldo = sueldo;
	}

	public Double getRetenciones() {
		return retenciones;
	}

	public void setRetenciones(Double retenciones) {
		this.retenciones = retenciones;
	}

	public Double getImpuestos() {
		return impuestos;
	}

	public void setImpuestos(Double impuestos) {
		this.impuestos = impuestos;
	}

	public Double getPremios() {
		return premios;
	}

	public void setPremios(Double premios) {
		this.premios = premios;
	}
	
	

}
