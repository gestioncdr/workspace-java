package ejercicio5;

import java.time.LocalDate;
import java.util.ArrayList;

abstract


public class ApliEjercicio5 {

	public static void main(String[] args) {
		
	//	LocalDate fechaNacimiento = LocalDate.of(1981, 1, 1);
	//	LocalDate fechaIngreso = LocalDate.of(2019,1,1);
		
		ArrayList<Empleado> empresa;
		
		empresa = new ArrayList<Empleado>();
		
		
		
		Trabajador trab1 = new Trabajador(1, "Juan", "CHOFER", "San Martin 232",  LocalDate.of(1981,1,1) ,  LocalDate.of(2019,1,1), 100d, 13d, 5d, 15d);
		Contratado cont1 = new Contratado();
		
		cont1.setNroLegajo(2);
		cont1.setNombre("L�pez");
		cont1.setCantidadHorasMes(10d);
		cont1.setTarifaHora(800d);
	/*	
		System.out.println(trab1.toString());
		System.out.println(cont1.toString() + "\n" +  "Sueldo: " + cont1.calcularSueldo());
	*/
		
		empresa.add(trab1);
		empresa.add(cont1);
		
		for (Empleado empleadoLeido : empresa) {
			System.out.println(empleadoLeido.toString() + "\n" +  "Sueldo: " + empleadoLeido.calcularSueldo());
		}
		
		
	}

}
