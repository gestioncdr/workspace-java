package ejercicio5;

import java.time.LocalDate;

	
public class Contratado extends Empleado {

	private Double tarifaHora;

	private Double cantidadHorasMes;
	//=== constructores ==================================
	public Contratado() {
		super();
		this.setTarifaHora(0d);
		this.setCantidadHorasMes(0d);
		
	}

	public Contratado(Integer nroLegajo, String nombre, String puesto, String direccion, LocalDate fechaNacimiento,
			LocalDate fechaContratacion, Double tarifaHora, Double cantidadHorasMes) {
		super(nroLegajo, nombre, puesto, direccion, fechaNacimiento, fechaContratacion);
		this.setTarifaHora(tarifaHora);
		this.setCantidadHorasMes(cantidadHorasMes);
	}

	
	@Override
	public String toString() {
		return super.toString() + "\n"+ "Contratado [tarifaHora=" + tarifaHora + ", cantidadHorasMes=" + cantidadHorasMes + "]";
	}

	// ===== setters y getters ==============================
	public Double getTarifaHora() {
		return tarifaHora;
	}

	public void setTarifaHora(Double tarifaHora) {
		this.tarifaHora = tarifaHora;
	}

	public Double getCantidadHorasMes() {
		return cantidadHorasMes;
	}

	public void setCantidadHorasMes(Double cantidadHorasMes) {
		this.cantidadHorasMes = cantidadHorasMes;
	}

	@Override
	public Double calcularSueldo() {
		// TODO Auto-generated method stub
		return this.getCantidadHorasMes() * this.getTarifaHora();
	}

	
}
